// Wait for the DOM to load before executing JavaScript
document.addEventListener("DOMContentLoaded", function () {
    // Hide all sections except the Home section by default
    const sections = document.querySelectorAll("section");
    sections.forEach((section) => {
      if (section.id !== "home") {
        section.style.display = "none";
      }
    });
  
    // Function to show the selected section and hide others
    function showSection(sectionId) {
      sections.forEach((section) => {
        if (section.id === sectionId) {
          section.style.display = "block";
        } else {
          section.style.display = "none";
        }
      });
    }
  
    // Get the navigation links and add event listeners to show the corresponding sections
    const navLinks = document.querySelectorAll("nav a");
    navLinks.forEach((link) => {
      link.addEventListener("click", function (event) {
        event.preventDefault();
        const targetSectionId = link.getAttribute("href").substring(1);
        showSection(targetSectionId);
      });
    });
  });
  